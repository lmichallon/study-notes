# Vue Router

Évite le raffraîchissement de la page au changement d'URL (c'est un système indépendant)\
Permet la manipulation de l'historique

Pour l'installer sur un projet :
```shell
vue add vue-router
```
*Attention* cette commande override le fichier *App.vue*\

Pour éviter ça (si on a travaillé sur ce fichier) on peut utiliser :
```shell
npm install vue-router
```

Ensuite on initialise le router dans *main.js* :
```js
import VueRouter from 'vue-router';

Vue.use(VueRouter); // pour intégrer le plugin dans mon instance de Vue.js
```

Il n'y a pas de notion de parent-enfant avec le router\

Le framework Vue.js n'est pas un MVC mais un MVVM : modèle-vue-vue-modèle\
Donc pas de contrôleur, les routes sont définies au niveau de la vue\

Pour définir toutes les routes, dans *src* on créé un dossier *routes* dans lequel on créé un fichier *router.js*\
On y importe vue-router puis on y créé les constantes routes et router, puis on exporte le router :

```js
import VueRouter from 'vue-router';

// les routes sont un tableau d'objets, où un objet est une route
const routes = [
    { path: '/login', component: Login, name: 'Login' },
    { 
        path: '/contact', 
        component: Contact, 
        name: 'Contact',
        meta: {
        requireAutg: true,
        toto: 42
    },
    { path: '/article/:id', component: Article, name: 'Article', props: true }, // pour props dans article il faut créer une props id
    { path: '/article/:id', component: () =>  import('@/pages/Article.vue'), name: 'Article', props: true }
]

// pour utiliser le tableau routes dans une instanciation de Vue
const router = new VueRouter({
    routes,
});

// pour utiliser ce tableau dans une instanciation de Vue
const router = new VueRouter({
    mode: 'history';
    routes,
});

export default router;
```

*La dernière route renseignée dans le tableau permet de charger l'article uniquement s'il est renseigné dans ma route (lady loading)*\
*Donc en termes de performance il est préférable de mettre toutes les sous-pages dans une fonction*

Ensuite dans le fichier *main.js* on importe ce fichier *router.js* :
```import router from './router/router';```

Et on ajoute le router dans le *new Vue* :
```js
new Vue({
    router,
    render: (h) => h(App),
}).$mount('#app');
```

La constante *routes* n'est rien de plus qu'un tableau. C'est le router qui contient la logique et qui fait que ça fonctionne ! 

Ensuite dans *App.vue* (ou le composant *root*) il faut ajouter dans le template une balise ```<router-view></router-view>```\
C'est à cet endroit là que le composant sera chargé par le router

Avec le router, on n'utilise plus des balises ```<a></a>``` parce que leur attribut *href* raffraîchirait la page et donc reloarderait Vue\
On utilisera plutôt :
```javascript
<router-link to="/login">

// :to parce que to ne marche que vers des chemins (path)
<router-link :to="{ name: 'contact' }"> 
<router-link :to="{ name: 'article', params: { id: this.articleId } }">
```

*Utile en CSS* : le lien actif a une classe router-link-active

Pour les redirections, on peut utiliser dans nos méthodes : 
```javascript 
this.$router.push('/contact');
```
```push``` parce que ça *push* la route dans l'historique de navigation

Il en existe d'autres, comme ```replace``` qui remplacera tout l'historique par la route */contact* ou encore ```go```

Pour retirer le # des routes on peut utiliser ```mode: 'history'```\
Mais il n'y aura plus de page 404 - il faudra les faire à la main avec un composant dédié\
Aussi, le router back-end pourra capter cette route, donc pour un même nom de route il prendra le dessus (ce qui ajoute de la config côté back-end)

## Cycle de vie

Le router a aussi un cycle de vie qui se déroule avant ou après les changements de route
Il y a donc des *events* beforeRouteLeave, beforeEnter (cf. la [documentation Vue.js](https://router.vuejs.org/guide/advanced/navigation-guards.html#the-full-navigation-resolution-flow))\
Par exemple beforeEach exécutera une fonction avant chaque changement de route - très utile pour faire du *firewall*

Pour l'utiliser on va dans le fichier *router.js* et avant de l'exporter : 
```js
const authenticated = false;

router.beforeEach((to: Route, from: Route, next: Fonction) => {
    if(to.name != 'login') { // si là où je vais n'est pas login, donc toutes les autres pages
        next('/login');
    } else {
        next();
    }
});
```
