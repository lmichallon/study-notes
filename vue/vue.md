# Vue.js

## Methods :

Leur objectif : créer des fonctions qui éxecutent du code\
*Elles n'ont pas un rôle d'affichage*

```js
methods: {
    getFirstName() {
        return 'Liswag';
    },
},
```

## Computed :

C'est dans *computed* qu'on gère ce qui est affiché (des choses qui sont calculées, des conditions...)\
On l'utilise comme une méthode, à la seule différence que ce qui est dans *computed* est mis en cache (c'est donc plus efficace pour l'affichage)

```js
computed: {
    displayAge(age) {
        return `${age} ans`;
    },
},
```

## Watch :

Un *watcher* va regarder chaque changement de valeur et éxecuter le code associé\
Il se déclenche à chaque changement et ce peu importe d'où ils viennent (même d'un parent éloigné)\
On peut les mettre uniquement sur des *data* et des *props*

```js
watch: {
    a: () => {
        console.log('coucou');
    },
    b: () => {
        if(this.b === 5) {
            console.log('cinq');
        } else  {
            console.log('prout');
        }
    }
}
```

## Mixins :

C'est une objet que l'on va pouvoir réutiliser dans chaque composant si on l'importe\
Ça peut être un fichier JavaScript simple, pas forcément un composant Vue\
Il a la même structure qu'un composant : toutes les fonctions de cycle de vie etc. peuvent y être utilisées à l'exception de *name*


```js
const maMixin {
    data () {
        return {
            title: '',
        };
    },
    methods: {
        getFirstName() {
            return 'Liswag';
        },
    },
    computed: {
        displayAge(age) {
            return `${age} ans`;
        },
    },
};

export default maMixin;
```

Pour l'utiliser dans un composant, on l'importe d'abord entre les balises *script*
puis on ajoute à l'*export default* ```mixins: [maMixin],```

## Filters :

Ils servent à transformer/formatter les données.\
Ils ne sont pas prédéfinis dans Vue.js, on doit les faire à la main\
Ils retournent forcément une valeur

```js
    filters: {
        upper: (value) => value.toUpperCase(),
        lower: (value) => value.toLowerCase(),
    },
```

Pour les utiliser dans un *template* : ```{{ description | upper | lower }}```\
On peut en utiliser plusieurs, ils s'appliqueront de gauche à droite

Ces filtres sont moins puissants que les *computed* car ils s'appliquent surtout sur des valeurs simples (strings, entiers... pas des tableaux par exemple)

## this.$

Pour intervenir dans le DOM en Vue.js on utilise plus ```document.``` mais ```this.$el``` \
```this.$el``` correspond au DOM physique (donc celui qu'on voit vraiment, pas le virtuel)

Pour accéder aux data etc. d'un composant parent on peut utiliser ```this.$parent```\
Pour accéder aux data etc. du composant parent le plus haut on peut utiliser ```this.$root```

 ```js
    // dans created() à titre d'exemple mais pourraient être utilisés ailleurs
    created() {
        this.$el.getElementById('');
        this.$el.querySelector('#app');

        // à utiliser le moins possible
        this.$root
        this.$parent
    }
 ```
 