# Utiliser le terminal

Un terminal est un type d'interface entre les composants et l'utilisateur sur un ordinateur.\
*C'est une des premières interfaces à avoir été créée.*\
C'est un endroit où on donne des commandes à l'ordinateur, qui les exécute.

En réalité sur un ordinateur on n'accède pas directement au terminal mais à un émulateur de terminal. Il en existe plusieurs types, parmi lesquels *bash* ou *zsh* par exemple.

Pour un terminal de type **bash** on a différents éléments :
- un **prompt** : c'est la première partie, au tout début.\
son style peut être différent selon la personnalisation de l'interface.\
ce prompt contient lui-même plusieurs éléments :
    - le **nom du login** : soit l'utilisateur sur lequel on est connecté actuellement
    - **@adresse** ou nom de la machine/de l'ordi physique
    - **~** qui indique dans quel dossier on se trouve actuellement
    *le terminal permet de naviguer dans les différents dossiers*
    - **$** qui indique la fin du prompt  
    tout ce qu'il y a après = **interface de commande**

Donc pour utiliser un terminal on écrit une commande puis on appuie sur entrée, sachant qu'on peut écrire qu'une seule commande par ligne.

## Commandes utiles :

- On annule une commande avec **Ctrl + C**
- On remonte/redescend dans son historique de commandes avec **↑** et **↓**
- On nettoie son terminal (mais pas son historique) avec **clear**
- On copie/colle avec **Ctrl + Shift + C** et **Ctrl + Shift + V**
- **ls** liste les fichiers du dossier dans lequel on se trouve\
On peut lui ajouter des options avec **--** ou **-**\
par exemple :
    - **ls -l** donne la même liste mais avec plus d'informations (date et heure, droits d'accès). *Le **-l** veut dire **long***
    - **ls -a** affiche les fichiers cachés. *Le **-a** veut dire **all***\
    Ces options peuvent être cumulées. par exemple :
        - **ls -la** pour afficher les fichiers cachées *et* davantage d'informations
        - **ls *nomDuDossier/*** pour lister les fichiers d'un autre dossier
- **pwd** permet d'afficher le chemin jusqu'au dossier où je suis actuellement
- **/** c'est la racine du disque dur. Donc si on commence une commande par / on part du début du disque dur dans notre arborescence, *sinon on part de l'endroit où on se trouve*.

**À savoir :** tout est un fichier sous Linux !\
**./** c'est le fichier actuel (donc écrire **./Downloads** revient à écrire **Downloads**)\
**../** c'est le dossier parent donc celui qui est juste au-dessus\
**../../** remonte de deux dossiers dans l'arborescence

**Attention :** le terminal est **sensible à la casse** !

- **cd** permet de naviguer dans notre ordinateur/parcourir les dossiers\
On utilise cette commande pour changer d'endroit, *cd* étant l'endroit où on veut aller.\
Par exemple pour aller dans le dossier *Music* : **cd Music**.\
Sans argument **cd** fait retourner à *Home*
- **file** permet d'avoir des informations sur un fichier.\
Par exemple **file Music** indiquera *directory*

Pour **manipuler des fichiers via le terminal** :\
***Attention** quand on utilise le terminal pour supprimer des documents il n'y aucun retour en arrière possible, la suppression est définitive.*
- **mkdir** créé un dossier. ***mk** pour **make** et **dir** pour **directory***
- **mkdir nomDuDossier** pour créer un dossier nommé *nomDuDossier*
- **mkdir Bonsoir Paris** créé deux dossiers nommés respectivement Bonsoir et Paris (l'espace sépare les arguments)
- **mkdir "Bonsoir Paris"** ou **mkdir Bonsoir\Paris** pour ne créer qu'un seul dossier Bonsoir Paris
- **rmdir** pour supprimer un dossier enfant qui n'a rien dedans. ***rm** pour **remove***\
Cette commande ne fonctionne pas si le dossier contient des sous-dossiers enfants.
- **touch** pour créer un fichier vierge\
**touch "bonsoirParis.mp3"** pour créer un fichier vierge nommé *bonsoirParis.mp3*
- **cp** pour copier un fichier.\
On spécifie sa source (donc le nom du fichier à copier) et sa destination (chemin + nom du fichier copié).\
Par exemple :
    - **cp bonsoirParis.mp3 bilalHassani.mp3** essayera de copier le fichier dans le dossier actuel
    - **cp bonsoirParis.mp3 /home/username/Downloads/bilalHassani.mp3** le copiera dans le fichier Downloads sous le nom *bilalHassani.mp3*
    - **cp -r ../Downloads** copiera tout le dossier Downloads. ***-r** pour la **récursivité***
- **mv** pour déplacer un fichier. ***mv** pour **move***\
Elle fonctionne comme la commande **cp**
- **cat** pour afficher tout le contenu d'un fichier
- **head** pour n'en afficher que le début\
**head -n 3** pour les trois premières lignes
- **tail** fonctionne de la même façon mais depuis la fin d'un fichier
- **cat text.html > result.txt** pour mettre le résultat d'une commande dans un fichier
- **cat text.html >> result.txt** pour le copier à la fin du fichier\
C'est utilisé avec **cat** ici mais ça fonctionne avec toutes les commandes qui ont un résultat. Si le fichier cible n'existe pas il est créé automatiquement.

**Les permissions :**\
Il y a deux utilisateurs sur un ordi : nous et "root" qui est le **superadmin**. Contrairement à nous, le superadmin peut tout faire, c'est-à-dire accéder à et supprimer n'importe quel fichier.

Chaque utilisateur et chaque groupe gère des permissions :
- **r** pour **read**
- **w** pour **write**
- **x** pour **execute**

Ces permissions sont donc gérées sur trois types de personnes :
- **u** pour **user**
- **g** pour **group**
- **o** pour **other**
- **a** pour **all**

Donc on règle le *read-write-execute* pour chaque *user-group-other* :
- **chmod** pour changer les permissions d'un fichier
- **chmod u** pour spécifier la cible du changement de permission
- **chmod g+w** pour ajouter les droits d'écriture
- **chmod g-w** pour ôter les droits d'écriture
- **chmod g+w style.css** pour indiquer le fichier concerné par la modification

C'est aussi possible en octal : **chmod 700 style.css**\
Les trois chiffres correspondent à lecture - écriture - exécution

- **sudo** permet de devenir superadmin le temps d'une commande
- **dnf** est un gestionnaire de paquets qui permet d'installer/supprimer des programmes.\
Les commandes **dnf** doivent être exécutées avec **sudo**
- **sudo dnf update** pour proposer des mises à jour
- **sudo dnf install firefox** pour installer firefox
- **sudo dnf remove firefox** pour le désinstaller
- **shutdown** pour éteindre son pc
- **reboot** pour le redémarrer
- **--help** à la suite d'une commande pour voir les arguments possibles et connaître leur utilité