# **Node Package Manager**

**dnf** est le gestionnaire de paquets de **Fedora**\
Celui de **JavaScript** (ou plus précisément **Node.js**) est **npm** - *pour node package manager*

Un gestionnaire de paquet sert à gérer ses dépendances. Une dépendance est une librairie externe, comme *Bootstrap* ou *jQuery*.

Il existe d'autres gestionnaires de paquets : **Bower**, **Yarn**...\
À noter que l'hébergement de Yarn passe forcément par Facebook donc pas ouf pour les données perso...

Il suffit d'entrer une commande pour les installer, les supprimer ou les mettre à jour.

Toutes les dépendances installées sont listées dans un fichier appelé **package.json**.\
Il faut mettre ce fichier sur **git** si on veut que d'autres installent les mêmes dépendances que nous (ça évite d'avoir le faire à la main).

Ce fichier **package.json** vient avecu n autre fichier appelé **package-lock.json**.\
Il est aussi à mettre sur git - c'est une fichier de fixation.\
On peut modifier le **package.json** mais pas le **package-lock.json**.

## **Pour initialiser un projet avec npm :** 
```shell
npm init
```
C'est cette commande qui créera le fichier **package.json**.\
On peut checker sa création avec la commande ```ls```

Ensuite on peut commencer à installer ses dépendances.

## **Pour installer une dépendance :** 
```shell
npm install <package-name>
```
Donc pour installer *Bulma*, un framework CSS, j'utiliserai la commande : ```npm install bulma```.

À noter que **npm** marche par projet (donc **package.json** par **package.json**).\
Donc ces commandes ne s'appliquent qu'au projet en question.

Maintenant si on utilise la commande ```ls``` on voit qu'un dossier **node_modules** a été créé.\
C'est dans ce dossier que sont installées les dépendances.\
Ce **node_modules** ne doit pas être mis sur **git** parce qu'il est trop lourd.\
On doit donc le lister dans le fichier **.gitignore**

### **dependencies et devDependencies**

Si par exemple je veux installer *Mocha*, un framework JavaScript qui sert aux tests unitaires :\
C'est un ficheir qui est utile au développeur mais pas à l'utilisateur.\
Donc il n'a rien à faire dans le fichier global des dépendances.

En utilisant la commande :
```shell
npm install mocha --save-dev
```
J'installe la dépendance *Mocha* uniquement sur l'environnement de développement (donc dans **devDependencies**).

Du coup, quand on livre en production, il suffit de spécifier de ne pas installer les **devDependencies** - *parce que sinon **npm** prend les deux (dependencies + devDependencies) par défaut*. Pour spécifier de ne pas installer ces devDependencies, liste dans le fichier **.gitignore** les dépendances à ignorer.

## **Pour mettre à jour toutes les dépendances et sous-dépendances :**
```shell
npm update
```
Attention cependant, ça peut être long car cette commande va comparer les versions pour chaque dépendance.

## **Pour ne mettre à jour qu'une seule dépendance :**
```shell
npm update <package-name>
```
Par exemple, pour mettre à jour *Bulma* : ```npm update bulma```\
On peut récupérer le nom des dépendances dans le fichier **package.json**.

