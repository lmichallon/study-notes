# **JavaScript**



## **Les opérations arithmétiques**



```javascript
pour les nombres :                  + - * / %
pour les strings :                  + sert à la concaténation       // sachant qu'une string prend le dessus sur un nombre (?)

maVariable = maVariable + 10;       // toutes les écritures font la même chose
maVariable += 10;

maVariable = maVariable + 1;        // toutes les écritures font la même chose
maVariable += 1;
maVariable++; ```
```



## **Les types de données**



- ```32``` est un ```INTEGER``` *(un entier)*

- ```'hello'``` est une ```STRING``` *(une chaîne de caractères)*

- ```true``` et ```false``` sont des ```BOOLEAN``` *(un booléen est vrai ou faux)*

- ```[42, 'Lisa']``` est un ```ARRAY``` *(un tableau)*

- ```{name: 'Lisa'}``` est un ```OBJECT``` *(un objet avec une clé **name** associé à la valeur **Lisa**)*

  

- ```null``` est la valeur par défaut donnée à une variable si déclarée comme suit : ```let maVariable```

-  ```undefined``` est la valeur par défaut donnée à une variable si déclarée comme suit : ```maVariable```

  

- ```typeof maVariable``` renvoie le type de la variable

  

- ```function maFonction();``` est une fonction appelée maFonction



### **Les template strings**





```javascript
let user = 'Lisa';
let message = 'Bonjour ' + user + ', bienvenue !';
let messageTemplate = `Bonjour ${user}, bienvenue !`;       // permet de sauter des lignes, utile pour les grosses manip'
```



### **Les booléens**



```javascript
&&      'ET'        true && true            // tout est vrai
||      'OU'        false || false          // au moins 1 est vrai
!       'NON'       !true --> false         // inverse : s'il était true, il devient false et vice-versa

(true && false) || (true || true);
```



### **Les tableaux**



```javascript
let array = ['Thomas', 'Cédric', ['Marie', 'Cassandra']]
array[2][0]                                                 // [2] récupère le deuxième tableau, [0] récupère 'Marie'


let array = ['Thomas', 'Cédric']

array.push('Panama');                   // pour ajouter un élément au tableau   -->   ['Thomas', 'Cédric', 'Panama']

array.slice(0,1);                       // ['Thomas']
array.slice(0,2);                       // ['Thomas', 'Cédric']

array.splice();                         // comme slice mais écrase le tableau d'origine
```



### **Les objets**



```javascript
let person = {          // permet de définir l'objet        ça s'appelle un JSON je crois ?
    'age': 25,
    'taille': 170,      // les guillemets sont pas obligatoires
    'poids': 70
}

person.age;                     // pour accéder à la donnée en question : ici l'âge de la personne
person['age'];                  // autre syntaxe possible mais moins utilisée

console.log(person.age);

person.taille = 171 ;    // pour changer la taille de la personne



let input = document.getElementById('test');
console.log(input.value)        // permet de récupérer la valeur d'un input
```



### **Les fonctions**



```javascript

function maFonction() {
    let name = 'Lisa';
    console.log(name);
}

maFonction();

    // pour faire varier l'utilisation de la fonction on peut lui donner un paramètre : ici le NOM
function maFonction(name) {             // name n'est défini qu'à l'intérieur de la fonction !
    console.log(name);
}

maFonction('Lisa');                     // name est remplacé par la donnée, c'est comme si j'avais name = 'Lisa'


function maFonction(name, age, gender) {}
maFonction('Lisa', 23, 'F');

setInterval (maFonction, 1000, 'Lisa', 23, 'F');       // paramètres de la fonction placés après les millisecondes

setInterval(function(){
    maFonction('Lisa', 23, 'F');
});

function addition(a, b) {
    let resultat = a + b;
    return resultat;                        // renvoie le résultat uniquement dans la fonction
}

addition(2, 4);
let resultatAddition = addition(2, 4);      // on met le résultat dans une variable pour le sortir de la fonction
```



#### **Les fonctions fléchées **:

```javascript
function maFonction(param1, param2) {
    return 'coucou';
}

maFonction();

const maFonction = function(param1, param2){        // déporte le nom de la fonction dans une variable
    return 'coucou';
}

maFonction();

const maFonction = (param1, param2) => {            // la flèche remplace 'function'
    return 'coucou';
}

const maFonction = (param1, param2) => 'coucou' ;   // qd on utilise pas d'accolades, ce qu'il y a après => est le return

maFonction();

    // pour une fonction sans paramètres :

function maFonctionSP() {
    return 'coucou';
}

    // devient

const maFonctionSP = () => {
    return 'coucou';
}
```



### **Les conditions**



#### **Si... sinon...**

```javascript
if('condition') {
    // code à exécuter si la condition est VRAIE
} else {
    // code à éxécuter si la condition est FAUSSE
}

==                  // égalité
===                 // égalité + égalité du type de donné
!=                  // inégalité
!==                 // inégalité + inégalité du type
>   <   <=  >=


let maVariable = '42';  

if(maVariable == 42) {}        // égalité fonctionne car indifférent au type
if(maVariable === 42) {}       // égalité ne fonctionne pas car type différent : '42' est une string alors que 42 est un entier
```



##### **L'opérateur ternaire** :

Il est utilisé pour faire de l'affectation 

```javascript
let bonjour = true;
let res;

if (bonjour == true) {
    res = 'coucou';
} else {
    res = 'au revoir';
}

let res = bonjour ? 'coucou' : 'au revoir';         // fait la même chose mais en une seule ligne

let res = bonjour ?? 'coucou';
```



#### **Selon**...

```javascript
switch (variableATester) {          // ne teste que des égalités (===)
    case 42:
        // code à exécuter si variableATester === 42
        break;
    case 54: 
    case 58:
        // code à exécuter si variableATester === 54 ou 58
        break;
    default:
        // code à exécuter par défaut
        break;  
}
```



### **Les boucles**



#### **Tant que...**

```javascript
// les boucles

while(condition) {
    // code à exécuter
}

let variableATester = 0;

while(variableATester <= 10) {
    console.log('blablabla');
    variableATester = variableATester + 1;          // variableATester doit forcément changer à l'intérieur de la boucle
}                                                   // sinon la boucle est infinie

do {
    // code à exécuter
} while (test <= 10);   // permet de faire une boucle qui s'exécuter au moins une fois, peu importe la valeur de variableATester
```



#### **For...**

```javascript
for (let i = 0; i <= 10; i = i++) {}                   // for (valeur de départ ; à exécuter tant que ; pas)
```



## **Écouter des événements**



Pour détecter l'action d'un·e utilisateur·rice sur le DOM on utilise un écouteur d'événement sur l'élément concerné.\
Par exemple, pour exécuter une fonction au clic sur un bouton :

```javascript 
// on va chercher dans le DOM les éléments à écouter ou modifier

const button = document.getElementById('button');
const title = document.getElementById('title');

// on applique un écouteur d'événement à l'élément voulu
// en spécifiant l'action déclencheuse et la fonction à exécuter

button.addEventListener('click', function(event) {

	console.log(event.target); // on check l'élément ciblé par le clic
	title.style.color = 'red'; // on change la couleur du titre depuis le fichier JS

});
```

Pour exécuter une fonction lorsqu'on appuie sur une touche de son clavier :

```javascript
window.addEventListener('keydown', function(event) {

    // on identifie la touche du clavier concernée grâce à son code
    // ici il s'agit de la touche Entrée

    if (event.keyCode === 13) {
        console.log('TOTO');
    }
});
```

Pour connaître le code d'une touche : [keycode.info](https://keycode.info/)

On peut aussi déclarer la fonction à déclencher à un autre endroit pour l'isoler et améliorer la syntaxe :

```javascript
// on déclare la fonction changeColor()

function changeColor() {
    const title = document.getElementById('title');
    title.style.color = 'red';
}

// on l'applique à un écouteur d'événement qui l'exécutera au clic sur un bouton

const button = document.getElementById('button');
button.addEventListener('click', changeColor);
```



## **Utiliser des *timers***



### **La méthode *setTimeout()***


Pour retarder l'exécution d'une fonction, on peut utiliser la méthode **setTimeout()** :

```javascript
const timer = window.setTimeout(function() {
    console.log('coucou');
}, 2000); 
```

Ici 2000 est l'intervalle de temps choisi en millisecondes (donc secondes x 1000).\
Ce timer affichera donc ***"coucou"*** dans la console 2 secondes après le chargement de la page.

Pour arrêter ce timer, on utilise :

```javascript
window.clearTimeout(timer);
```

C'est particulièrement utile si on l'utilise dans ce genre de fonction :

```javascript
button.addEventListener('click', function() {
    window.clearTimeout(timer);
});
```



### **La méthode *setInterval()***

Pour exécuter une fonction toutes les 3 secondes par exemple, on utilisera la méthode **setInterval()** :

```javascript
const interval = window.setInterval(function() {
    console.log('coucou');
}, 3000);
```

Pour mettre fin à cet interval, on utilisera :

```javascript
window.clearInterval(interval); // le 'window' est optionnel
```



## **Le stockage des données**



```javascript
window.localStorage         // partagé entre les différents onglets
window.sessionStorage       // cloisonné dans l'onglet actuel

cookies                     // façon de stocker des données sur un ordi
/* mélange entre localStorage et sessionStorage : par défaut n'ont pas de durée de vie mais on peut leur en donner
donc localStorage sur lequel on met une durée de vie, en gros */
indexdDB                    // base de données locale sur un navigateur

window.localStorage.setItem('maVar', 'Thomas');       // fonction qui permet de stocker une chaîne de caractères, ici 'Thomas'
let prenom = window.localStorage.getItem('maVar');    // fonction qui permet de récupérer quelque chose qui a été stocké
window.localStorage.removeItem('maVar');              // supprime le stockage de 'maVar' mais pas l'affectation (prenom)
window.localStorage.clear();                          // supprime tout le stockage, reset

// on les utilise pour stocker des informations TEMPORAIRES et NON SENSIBLES

let chiffre = 4;
window.localStorage.setItem('maVar', chiffre.toString());   // pour transformer un chiffre en chaîne de caractères


/* pour créer des attributs personnalisés : dans un tag HTML écrire data-nomPersonnalisé=""
par exemple : data-nom="Michallon" data-prenom="Lisa"
aucun intérêt en HTML et CSS mais utile de le récupérer en JS */

let title = document.getElementById('title');
title.dataset // dataset pour avoir un tableau qui regroupe les différentes propriétés data de l'objet sélectionné
console.log(title.dataset.prenom);
title.dataset.prenom = 'Thomas';                      // modifie la valeur du dataset dans le code HTML
```



## **Les dates**



```javascript

let date = new Date();                              // pour avoir la date et l'heure de MAINTENANT
let date = new Date(1980, 10, 11, 10, 00, 00);      // pour cibler une date et heure précise, ici en 1980
let heures = date.getHours();                       // pour n'avoir que l'heure
let mois = date.getMonth();                         // pour n'avoir que le mois

let p = document.createElement('p');
p.innerText = date;                                 // affiche touuutes les infos de la date
p.innerText = date.toLocaleString();                // permet d'adapter le toString au format utilisateur
document.body.append(p);

console.log(date);
console.log(heures);
console.log(mois);

console.log(date.valueOf());                        
// nombre de millisecondes depuis le 1er janvier 1970 
// utile pour manipuler les dates askip car c'est toujours un entier
```



## **Les promesses**



```javascript
// toujours définies dans une variable
// crées avec 'new Promise()' et contiennent toujours une fonction comme paramètre
// cette fonction a deux options : resolve & reject
// resolve = tout s'est bien passé      reject = il y a eu une erreur

let promise = new Promise (function(resolve, reject) {
    console.log('start');
    setTimeout(function() {         // resolve = paramètre de retour, fonctionne comme un return
        resolve('ok')               // quand les 2 secondes sont passées, alors la promesse est résolue
    }, 2000);                       // on peut aussi utiliser 'reject()' au lieu de 'resolve()'
});

promise.then(function(value) {      // .then() est une fonction
    console.log('coucou')           // exécute promise et ensuite seulement fait qch (qch étant une fonction)
});

function longProcess() {
    setTimeout(function() {
        console.log('longP')
    }, 2000);
}

longProcess();
console.log('afterLong'); 

// pending, fullfilled, rejected, settled --> définit les différents états de ma promesse
// en cours de traitement, finie et ok, finie et pas ok, finie (peu importe si ok ou pas ok)

// fonction .catch pour gérer uniquement les erreurs

// le résultat d'une promise est une promesse en elle-même donc on peut les chaîner : process.then().then();

Promise.all([promise1, promise2]).then(function() {     
    console.log('ok');
})                                                      // tableau de promise pour attendre que ttes les promises soient terminées


Promise.race([promise1, promise2]).then(function() {     
    console.log('ok');
})          // lance toutes les promesses d'un coup puis s'exécute qd au moins une promesse est terminée
```



## **Fetch**



```javascript

XMLHttpRequest
$.ajax
axios

// FETCH
// le résultat de fetch() est une promesse
//  ne marche pas si on ne passe pas par un serveur

fetch('').then()        // fetch() c'est le côté requête    then() c'est le côté réponse

fetch('https://reqres.in/api/users', {
    method: 'POST'      // modifie le paramètrage car fetch est en GET par défaut
    body: {
        email: 'thomas'
    }
})
    .then(function(response){
        // étape 1 : reçoit la réponse
        console.log(response);
        response.text();            // chaîne de caractères
        response.json();            // aussi pour du texte mais ordonné
        response.blob();            // pour tout ce qui est binaire (ex:img) donc pas de représentation en chaîne de caractères
        response.formData();        // pour les données envoyées par un formulaire car elles ont une forme spécifique
        return response.json();
    })
    .then(function(transformation) {
        // étape 2 : transforme la réponse  (uniquement le corps / body de la réponse)
        console.log(transformation);
        console.log(transformation.data[0]);
    })



const chat = document.getElementById('chat');       // sur l'HTML id placé sur une balise <img>

fetch('https://avataaars.io/?avatarStyle=Circle&topType=WinterHat3&accessoriesType=Round&hatColor=White&facialHairType=BeardMedium&facialHairColor=Brown&clotheType=Hoodie&clotheColor=PastelRed&eyeType=Surprised&eyebrowType=AngryNatural&mouthType=Serious&skinColor=Tanned')
    .then(function(response) {
        // étape 1
        if(response.ok === true) {
            return response.blob();
        } else {
            return new Error ('ça marche pas');
        }
    })
    .then(function(transformation) {        // la fonction peut s'appeler autrement que 'transformation'
        // RESOLVE
        // étape 2
        console.log(transformation);

            let cheminDuFichier = URL.createObjectURL(transformation);      // createObjectURL renvoie une string
            console.log(cheminDuFichier);

            chat.src = cheminDuFichier;
        }, 

        function(error) {
            // REJECT
        }
    )
```