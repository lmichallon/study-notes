# **Les expression régulières**

En JavaScript tout ce qui commence *et* se termine par un slash est une expression régulière.\
Par exemple : **/abc/** capte la chaîne de caractères "abc" dans un texte.

Ce qui est entre les slash est considéré comme un seul mot.\
Même si on utilise un espace, il sera compté comme un caractère et non comme un séparateur.

Pour former des groupes, on utilise des crochets :
**[abc]**.\
Donc pour capter tous les a, les b et les c dans un texte on utilisera **/[abc]/**.

Pour créer un range on utilise un tiret dans des crochets.\
Par exemple, **[a-z]** captera tous les caractères de A à Z en minuscules.\
**[a-zA-Z0-9]** captera tous les caractères de A à Z en minuscules, en majuscules ainsi que les chiffres.

Pour capter un mot = une suite de caractères alpha-numériques *+ underscore* : **/\w/**\
Donc *abc*, *a56V*, *toto*... seront considérés comme des mots.

À l'inverse, pour ne pas capter les mots on utilisera : **/\W/**.\
Cela permettra de capter les espaces, les points, les virgules...

Pour ne capter que les espaces : **/\s/**\
Pour ne pas capter les espaces : **/\S/**

Si j'utilise **/Lorem ipsum/** ce sera considéré comme un seul mot.\
Pour que ce soit considéré comme deux mots, il faudra utiliser : **/Lorem\sipsum/**.

Si je veux capter un motif "lettre espace lettre" : **/\w\s\w/**

Pour ne capter que les chiffres : **/\d/**\
Pour ne pas capter les chiffres : **/\D/**

Le point **.** en regex correspond à *tout* (n'importe quel caractère).\
Pour tout capter *sauf* tels caractères j'utilise un **^** dans un groupe.
Par exemple, pour rechercher l'inverse de abc *donc tout sauf les lettres a, b et c* : **/[^abc]/**

On utilise une accolade après un caractère pour signifier que l'on veut trouver un caractère *tel* nombre de fois :
- **/a?/** pour rechercher "a" 0 ou 1 fois
- /a*/ pour rechercher "a" au moins 0 fois (de 0 à ∞)
- **/a+/** pour rechercher "a" au moins 1 fois (de 1 à ∞)
- **/a{2}/** pour rechercher "a" 2 fois ("aa")
- **/a{10,}/** pour rechercher "a" au moins 10 fois (de 10 à ∞)

Le quantifieur ne marche que sur le caractère précédent !\
Donc **/ol{3}/** captera *"olll"* et pas *"ololol"*\
Si je veux capter *"ololol"* il faudra utiliser **/(ol){3}/**\
Et pour rechercher *"ololol"* OU *"acacac"* on utilise **/(ol|ac{3})/**

Si je veux rechercher quelque chose qui commence par abc : **/^abc/**\
Si je veux rechercher quelque chose qui se termine par abc : **/$abc/**

## **Leur utilisation en JavaScript**

### **Les méthodes :**

- #### **La méthode [test()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/RegExp/test) :**

Elle vérifie s'il y a une correspondance entre un texte et une expression rationnelle.\
Elle retourne true en cas de succès et false dans le cas contraire.

```js
'abc'.test(/abc/) // renvoie true
'dd'.test(/abc/)  // renvoie false
```

- #### **La méthode [match()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/match) :**

Elle permet d'obtenir le tableau des correspondances entre la chaîne courante et une expression rationnelle.

```js
'abc'.match(/abc/) // renvoie un tableau des occurences trouvées
```

- #### **La méthode [search()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/search) :**

Elle éxecute une recherche dans une chaine de caractères grâce à une expression rationnelle appliquée sur la chaîne courante.

```js
'pppabcppp'.search(/abc/) // renvoie un entier : position des caractères recherchés dans la chaîne
```

- #### **La méthode [replace()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/replace) :**

Elle renvoie une nouvelle chaîne de caractères dans laquelle tout ou partie des correspondances à un modèle sont remplacées par un remplacement. \
La chaîne de caractère originale reste inchangée.

```js
'pppabcppp'.replace(/abc/,'toto') // recherche "abc" et le remplace par "toto"
```

### **[Les flags *ou marqueurs*](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/RegExp) :**

On peut utiliser et cumuler des flags/marqueurs en argument.

- #### **Pour une recherche globale : le flag *g***
```js
'abc'.match(/abc/g)
```
Il permet de retrouver toutes les correspondances plutôt que de s'arrêter après la première.

- #### **Pour une recherche insensible à la casse : le flag *i***
```js
'abc'.match(/abc/i)
```
Si le marqueur **u** est également activé, les caractères Unicode équivalents pour la casse correspondent.

- #### **Pour une recherche multilignes : le flag *m***
```js
'abc'.match(/abc/m)
```

Pour tester la validité d'un champ d'email, on utilise **/(.+)@(\w+\.w+)/**\
Ça renverra *true* si la chaîne testée remplit ces conditions :
- au moins un caractère quel qu'il soit
- puis une arobase
- puis au moins une lettre
- puis un point
- puis au moins une lettre

Dans un fichier JS on peut définir une nouvelle RegExp avec :
```js
let regex = new RegExp('/abc/');
prenom.match(regex); // teste la correspondance avec la variable "prenom"
```

On peut aussi intégrer un pattern directement dans l'HTML, dans une balise  ```<input>``` mais ça fonctionne uniquement avec les types **textuels** :

```html
<input type="text" pattern="[abc]">
```