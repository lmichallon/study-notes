# **Fedora 33 Full Setup**

**Everything you need to code on a new [Fedora 33 Workstation](https://getfedora.org/en/workstation/download/)!**

To check your OS version:
```shell
cat /etc/fedora-release
```

Install **Tweaks**:
```shell
sudo dnf install gnome-tweaks
```

Some nice [GNOME Shell extensions](https://extensions.gnome.org/): [Dash to Dock](https://github.com/micheleg/dash-to-dock)

## **I - Linux, Apache, MySQL, PHP (LAMP)**

### **A - [Apache HTTP Server](https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-apache-http-server/)**

Install HTTPD packages:
```shell
sudo dnf install httpd -y
```

Start the HTTPD service:
```shell
systemctl start httpd.service
```

Enable auto start of HTTPD service at boot:
```shell
systemctl enable httpd.service
```

Navigate to [http://localhost](http://localhost) to access the Apache test page.

For configuration go to file **userdir.conf**:
```shell
cd
cd /etc/httpd/conf.d
sudo nano userdir.conf
```
- comment line **"UserDir disable"** 
- uncomment line **"#UserDir public_html"**

Create **public_html** directory and manage permissions :
```shell
cd
mkdir public_html
sudo chmod 711 /home/username
sudo chmod 755 /home/username/public_html
sudo setsebool -P httpd_enable_homedirs 1
```

Restart server:
```shell
systemctl restart httpd.service
```

Navigate to *[http://localhost/~username](http://localhost/~username)* to check your configuration.

### **B - [MySQL / MariaDB](https://www.itzgeek.com/how-tos/mini-howtos/securing-mysql-server-with-mysql_secure_installation.html)**

Install:
```shell
sudo dnf install mariadb-server
```

Start MariaDB using your distribution's init system:
```shell
sudo systemctl start mariadb
```

To enable MariaDB server to start upon boot: 
```shell
sudo systemctl enable mariadb
```

To improve the security of MariaDB installation:
```shell
sudo mysql_secure_installation
```

Check with:
```shell
mysql --version
```

Access MariaDB client shell:
```shell
sudo mysql -u root
```

Create a user:
```shell
CREATE USER '<username>'@'localhost' IDENTIFIED BY '<password>';
```

Grant them all privileges:
```shell
GRANT ALL PRIVILEGES ON *.* TO '<username>'@'localhost';
```

Refresh privileges:
```shell
FLUSH PRIVILEGES;
```

List users:
```shell
SELECT User FROM mysql.user;
```

Check a user permissions :
```shell
SHOW GRANTS FOR '<username>'@localhost;
```

### **C - PHP**

Install:
```shell
sudo dnf install php php-cli php-mysqlnd php-xdebug
```

Check:
```shell
php --version
```

To test your installation, create an **info.php** file in your **public_html** directory:
```shell
cd public_html
touch info.php
nano info.php
```

And write:
```php
<?php phpinfo(); ?>
```

Then restart HTTPD service:
```shell
systemctl restart httpd.service
```

And navigate to *[http://localhost/~username/info.php](http://localhost/~username/info.php)*. Information about PHP's configuration should be displayed.

## **II - Coding Environment**

### **A - SSH key**

Generate SSH key:
```shell
ssh-keygen -t ed25519
```

Get your public key:
```shell
cat .ssh/id_ed25519.pub
```

Copy/paste it to your GitHub and GitLab settings.

Run:
```shell
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```

### **B - [Node Version Manager](https://github.com/nvm-sh/nvm#installing-and-updating)**

To install or update:
```shell
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.0/install.sh | bash
```
*It downloads and runs the install script.*

To verify that nvm has been installed:
```shell
command -v nvm
```
If the installation was successful, it should output **nvm**.

### **C - [Composer](https://www.hostinger.com/tutorials/how-to-install-composer)**

Download from official website:
```shell
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
```

Verify the installer's signature (**SHA-384**) to ensure that the file is not corrupt:
```shell
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
```

To install locally (current directory):
```shell
php composer-setup.php
```

To install globally:
```shell
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```

Remove the installer:
```shell
php -r "unlink('composer-setup.php');"
```

Check installation:
```shell
composer
```

### **D - [Symfony CLI](https://symfony.com/download)**

Install:
```shell
 wget https://get.symfony.com/cli/installer -O - | bash
```
Run with your own username:
```shell
sudo mv /home/username/.symfony/bin/symfony /usr/local/bin/symfony
```

Check :
```shell
symfony -v
```

### **E - IDE**

#### **i - Install [Snap](https://www.cyberciti.biz/faq/install-snapd-on-fedora-linux-system-dnf-command/):**
```shell
sudo dnf install snapd
```

Check with :
```shell
snap version
```

Create a symlink :
```shell
sudo ln -s /var/lib/snapd/snap /snap
```

Check with :
```shell
ls -l /snap
```

#### **ii - Install [VSCode](https://snapcraft.io/install/code/fedora):**
```shell
sudo snap install code --classic
```

*Useful extensions: Material Theme, Community Material Theme, Material Theme Icons, Rainbow Brackets, Alignment*

#### **iii - Install [PhpStorm](https://snapcraft.io/install/phpstorm/fedora):**

```shell
sudo snap install phpstorm --classic
```

*Useful plugins: Atom Material Icons, Atom One Dark, Material Theme UI, Rainbow Brackets*

### **F - [Customize Terminal](https://kifarunix.com/install-and-setup-zsh-and-oh-my-zsh-on-fedora-32/)**


Install Zsh:
```shell
sudo dnf install zsh
```

Check:
```shell
zsh --version
```

Make it your default shell:
```shell
sudo usermod -s $(which zsh) username
```
Then close your current terminal and open a new one.

Check default shell with:
```shell
echo $SHELL
```
It should now display **/usr/bin/zsh**

#### **i - Install [Oh My Zsh](https://ohmyz.sh) framework:**
```shell
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
```

#### **ii - Download [Powerlevel10k](https://github.com/romkatv/powerlevel10k) theme:**
```shell
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```
Then set **ZSH_THEME="powerlevel10k/powerlevel10k"** in **~/.zshrc**.

For configuration:
- either close your current terminal and open a new one, Powerlevel10k configuration wizard will ask you a few question to configure your prompt.
- or, if it doesn't trigger automatically, type ```p10k configure```

## **III - Other Apps**

### **A - Google Chrome**

Download Google Chrome (64bit):
```shell
wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
```

Install the downloaded package:
```shell
sudo dnf install google-chrome-stable_current_x86_64.rpm
```

*Run:*
```shell
google-chrome &
```

*Update:*
```shell
sudo dnf update google-chrome-stable
```

### **B - [Flatpak](https://flatpak.org/setup/Fedora/)**

Install:
```shell
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

#### **i - Install [Discord](https://flathub.org/apps/details/com.discordapp.Discord):**
```shell
flatpak install flathub com.discordapp.Discord
```

*Run:*
```shell
flatpak run com.discordapp.Discord
```

#### **ii - Install [Spotify](https://flathub.org/apps/details/com.spotify.Client):**
```shell
flatpak install flathub com.spotify.Client
```

*Run:*
```shell
flatpak run com.spotify.Client
```

**And you're (finally) done!**