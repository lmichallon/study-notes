# Créer des formulaires de connexion/inscription sur Symfony

## I - Créer la classe User

```shell
php bin/console make:user
```

Répondre :
- User
- yes
- email
- yes

Cette commande créé :
- **src/Entity/User.php** qui contient la classe User 
- **src/Repository/UserRepository.php**

Et met à jour :
- **config/packages/security.yaml**

C'est dans le fichier **config/packages/security.yaml** qu'on gère tout ce qui touche aux utilisateurs. On peut y ajouter des façons de s'authentifier (par exemple depuis Facebook ou Google), choisir l'algorithme qui encode les mots de passe etc.

## II - Créer la table User dans ma base de données 

```shell
php bin/console make:migration
```

```shell
php bin/console doctrine:migration:migrate
```

## III - Créer un formulaire de connexion

```shell
php bin/console make:auth
```

Répondre :
- 1
- LoginFormAuthenticator
- SecurityController
- yes

Cette commande créé : 
- **src/Security/LoginFormAuthenticator.php** contenant la fonction *onAuthenticationSuccess* qui permet de choisir la page sur laquelle l'utilisateur est redirigé une fois connecté
- **src/Controller/SecurityController.php** contenant les fonctions *login* et *logout*
- **templates/security/login.html.twig** qui est une page de login

## IV - Créer un premier User pour pouvoir se connecter

Pour ça rendez-vous dans la BDD ! On obtient un mdp encodé avec :
```shell
php bin/console security:encode-password
```

## V - Créer un formulaire d'inscription

```shell
php bin/console make:registration-form
```

Répondre :
- yes
- email
- yes

Cette commande créé : 
- **src/Form/RegistrationFormType.php** contenant le builder du formulaire d'inscription
- **src/Controller/RegistrationController.php** contenant la fonction *register*
- **templates/registration/register.html.twig** qui est une page d'inscription

Et met à jour :
- **src/Entity/User.php**

Pour attribuer un rôle USER à toute personne qui créé un compte :
dans la fonction **register** du fichier ***src/Form/RegistrationFormType.php*** ajouter

```php
$user->setRoles(['ROLE_USER']);
```

Les rôles attribués aux utilisateurs se gèrent dans le fichier ***config/packages/security.yaml*** depuis le **access control**. 

Après ce **access control**, on peut définir une hiérarchie des rôles en ajoutant au même niveau :
```php
role_hierarchy:
    ROLE_ADMIN: ROLE_USER
    ROLE_OWNER: [ ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH ]
```
Ici on définit :
- qu'un utilisateur avec le rôle ADMIN aura aussi le rôle USER
- qu'un utilisateur avec le rôle OWNER aura aussi le rôle ADMIN, et donc le rôle USER