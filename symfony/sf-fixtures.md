# Créer des fixtures

*Quand elles sont exécutées, les fixtures permettent d'injecter des données en BDD.*

*Si nécessaire :* 
```shell
composer require orm-fixtures --dev
```
## I - Créer une fixture
```shell
php bin/console make:fixtures
```

Pour modifier la table Job on nommera le fichier **JobFixtures.php**

Cette commande créé un dossier **DataFixtures** dans ***src*** avec à l'intérieur :
- AppFixtures.php *(on peut le supprimer)*
- JobFixtures.php

Pour définir les données à injecter, dans **JobFixtures.php** :

```php
<?php

namespace App\DataFixtures;

use App\Entity\Job;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class JobFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $job = new Job();
        $job->setName('Développeur');
        $manager->persist($job);

        $job = new Job();
        $job->setName('Expert Cyber Sécurité');
        $manager->persist($job);

        $manager->flush();
    }
}
```

## II - Injecter les données en BDD :

Si on veut écraser le contenu de la BDD :
```shell
php bin/console doctrine:fixtures:load
```

Sinon, pour ajouter le contenu des fixtures à ce qui est déjà présent en BDD :
```shell
php bin/console doctrine:fixtures:load --append
```
## III - Pour utiliser la librairie **[Faker](https://github.com/fzaninotto/Faker)** :

*Elle permet de générer des données aléatoirement.*
```shell
composer require fzaninotto/faker
```

Si je veux générer des emplois aléatoires, dans **JobFixtures.php** je code :

```php
<?php

namespace App\DataFixtures;

use App\Entity\Job;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class JobFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // choix de la langue
        $faker = Factory::create('fr_FR');

        // génération de 10 emplois aléatoires
        for ($i = 0; $i <= 10; $i++) {
            $job = new Job();
            $job->setName($faker->jobTitle);
            $manager->persist($job);
        }
        $manager->flush();
    }
}
```