# Écrire en SQL

## I - La base de données

```sql
CREATE DATABASE Swag; -- créer une base de données
DROP DATABASE Swag; -- supprimer une base de données
```

## II - Les tables
### Pour créer une table :
```sql
CREATE TABLE country(
    id INT NOT NULL AUTO_INCREMENT,
    country_name VARCHAR(255) NOT NULL,
    capital VARCHAR(255) NOT NULL,
    population INT NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);
```

### Pour la supprimer :
```sql
DROP TABLE country;
```

### Pour la modifier :

- #### Ajouter une colonne :
```sql
ALTER TABLE country
ADD COLUMN langue VARCHAR(255) NOT NULL;
```

- #### Supprimer une colonne :
```sql
ALTER TABLE country
DROP COLUMN langue;
```

- #### Renommer une colonne :
```sql
ALTER TABLE country
CHANGE population country_population;
```

- #### Modifier une colonne :
```sql
ALTER TABLE country
MODIFY population BIGINT NOT NULL DEFAULT O;
```

## III - Les contraintes :

### A - Les clés étrangères :

- #### Définir une clé étrangère à la création de la table :
 ```sql
CREATE TABLE city (
    id INT AUTO_INCREMENT,
    city_name VARCHAR(255),
    country_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT FK_city_country -- FK_tableSource_tableCible
        FOREIGN KEY (country_id) -- (colonneSource)
        REFERENCES (country(id)) -- tableCible(colonneCible)
);
```

- #### Définir une clé étrangère après la création de la table :
```sql
ALTER TABLE city
ADD CONSTRAINT FK_city_country 
	FOREIGN KEY (country_id) 
	REFERENCES country(id);
```

- #### Supprimer une clé étrangère :
```sql
ALTER TABLE city
DROP CONSTRAINT FK_city_country;
```

- #### Permettre la suppression/modification en cascade :
```sql
CREATE TABLE city (
id INT AUTO_INCREMENT,
city_name VARCHAR(255),
country_id INT NOT NULL,
PRIMARY KEY (id),
CONSTRAINT FK_city_country 
	FOREIGN KEY (country_id) 
	REFERENCES country(id)
    -- si country_id est modifié ou supprimé
    -- toutes les données qui lui sont associées le seront aussi
	ON DELETE CASCADE
	ON UPDATE CASCADE
);
```

### B - Les clés uniques :

- #### Définir une clé unique à la création de la table :
```sql
CREATE TABLE user (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT UK_email UNIQUE KEY (email)
);
```

- #### Définir une clé unique après la création de la table :

```sql
ALTER TABLE user
ADD CONSTRAINT UK_email UNIQUE KEY (email);
```

## IV - Les données

### A - Pour ajouter des données :
```sql
-- pour insérer des données dans toutes les colonnes
INSERT INTO country VALUES 
(NULL, "Allemagne", "Berlin", 398439),
(NULL, "Espagne", "Madrid", 387439230);

-- pour ne cibler que certaines colonnes
INSERT INTO country(country_name, capital) VALUES
("Suède", "Stockholm");

```

### C - Mettre à jour des données :

```sql
UPDATE ville
SET libelle_ville = 'Marseille'
WHERE libelle_ville <> 'Marseille'
AND num_ville = 2;
```

- #### Supprimer une ligne entière :
 ```sql
DELETE FROM ville
WHERE num_ville = 2;
```

- #### Supprimer une case seulement :
 ```sql
-- à compléter 
```

### B - Pour sélectionner des données :

- #### Sélectionner toutes les colonnes d'une table :
```sql
SELECT * FROM country;
```

- #### Sélectionner des colonnes dans une table :
```sql
SELECT country_name, population 
FROM country;
```

 - #### Ordonner les données sélectionnées :
 ```sql
SELECT * 
FROM country
ORDER BY libelle_ville DESC; -- ASC par défaut
```

- #### Filtrer les données sélectionnées :

```sql
SELECT country_name, population 
FROM country
WHERE population >= 300000
AND country_name = 'Allemagne'
OR country_name != 'France';
```

```sql
SELECT num_ville
FROM ville
-- les villes qui commencent par un P
WHERE libelle_ville LIKE 'P%'
-- ou les villes qui commencent par un M suivi d'un seul caractère
OR libelle_ville LIKE 'M_';
```

```sql
SELECT num_ville
FROM ville
-- soit Paris, soit Lyon soit Toulouse
WHERE libelle_ville IN ('Paris', 'Lyon', 'Toulouse'); 
```

## V - Les sous-requêtes

 ```sql
INSERT INTO country VALUES (1, 'France');
INSERT INTO city VALUES (NULL, 'Paris', 1);
INSERT INTO city VALUES (NULL, 'Paris', (
	SELECT id 
	FROM country 
	WHERE country_name = 'France')
    );
```

```sql
SELECT toto.Art_code 
FROM (SELECT * FROM Artiste) AS toto
WHERE toto.Art_prenom = 'Jean-Louis';
```

 ```sql
SELECT Art_code
FROM Artiste 
WHERE Dis_code = (
	SELECT Dis_code 
	FROM Disque 
	WHERE Dis_titre = 'H'
    );
```
- #### Filtrer sa sous-requête :
*Utile quand une sous-requête renvoie plusieurs valeurs*


```sql
SELECT Art_code
FROM Artiste 
WHERE Dis_code = ANY(
	SELECT Dis_code 
	FROM Disque 
	WHERE Dis_titre = 'H');
```

On utilise ```ANY``` pour avoir **au moins une** valeur sortie par la sous-requête et ```ALL``` pour avoir **toutes** les valeurs sorties par la sous-requête.

## VI - Les jointures :

Elles permettent d'assembler deux tables sur un *SELECT*. On ne peut les utiliser que sur deux tables reliées par une **clé étrangère**

```sql
SELECT city.id, city_name, coutrny_name
FROM city
-- paramètres sur lesquels se fait la jointure
JOIN country ON city.country_id = country.id
```

*Pour une explication visuelle des jointures [[x](https://blog.codinghorror.com/a-visual-explanation-of-sql-joins/)]*

Par défaut elles sont des ```INNER JOIN```, c'est-à-dire qu'elles prennent *uniquement* les données en commun.

Les ```FULL OUTER JOIN``` prennent toutes les données.

Les ```LEFT OUTER JOIN``` prennent tout ce qui est relié + ce qui n'est pas relié dans la table définie dans le ```FROM```.

Les ```RIGHT OUTER JOIN``` prennent tout ce qui est relié + ce qui n'est pas relié dans l'autre table.

## VII - Les fonctions

### A - Compter

```sql
SELECT COUNT(Dis_Code)
FROM Disque;
```

```sql
SELECT COUNT(Dis_Code) AS comptage
FROM Disque
WHERE comptage > 5;
```

```sql
SELECT COUNT(*) -- compte sur la clé primaire
FROM Disque
WHERE comptage > 5;
```

```sql
SELECT *
FROM Artiste
WHERE COUNT(Art_code) < 5;
```

```sql
SELECT Art_nom, Art_prenom
FROM Disque
JOIN Artiste ON Disque.Art_code = Artiste.Art_code
GROUP BY Artiste.Art_code
HAVING COUNT(Dis_code) > 2; -- artistes qui ont sorti + de 2 albums
```

### B - Sommer

```sql
SELECT SUM(Dis_ventes)
FROM Disque;
```

```sql
-- sélectionne le code de l'artiste et la somme de toutes ses ventes d'albums
SELECT Art_code, SUM(Dis_ventes)
FROM Disque
GROUP BY Art_code;
```

```sql
SELECT Art_code
FROM Disque
GROUP BY Art_code
HAVING SUM(Dis_ventes) >= 3000000; -- artiste dont la somme des ventes >= 3000000
```

## VIII - Les transactions :

Les transactions permettent d'exécuter plusieurs requêtes en un seul bloc. Si une requête du bloc échoue, on peut ainsi choisir de ne pas exécuter les autres requêtes du bloc.

```sql
SET autocommit=0; -- retirer l'autocommit

COMMIT;   -- valider les requêtes
ROLLBACK; -- annuler les requêtes

START TRANSACTION; -- démarrer une transaction

COMMIT; -- valider les requêtes de la transaction
```


```sql
START TRANSACTION; -- début de la transaction

UPDATE compte
SET argent = 45
WHERE id = 1;

UPDATE compte
SET argent = 105
WHERE id = 2;

COMMIT; -- fin de la transaction
```

## IX - Les index :

Pour améliorer les performances, il est possible d'utiliser des ```INDEX``` qui réorganisent les tables. On les met sur des colonnes qu'on utilise beaucoup.

Par exemple, sur les ventes de disques :

```sql
CREATE INDEX idx_disque ON Disque(Dis_ventes);
CREATE INDEX idx_disque ON Disque(Dis_ventes, Dis_titre);
```
Ça permet d'augmenter les performances sur les ```SELECT``` mais ça les réduit sur les ```INSERT```. Cependant le gain est tellement important sur les ```SELECT``` que pour une base de données, utiliser des ```INDEX``` c'est la première chose à faire.